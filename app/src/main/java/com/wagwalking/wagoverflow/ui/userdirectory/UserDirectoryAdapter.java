package com.wagwalking.wagoverflow.ui.userdirectory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wagwalking.wagoverflow.R;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummary;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummaryListener;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummaryViewHolder;

import java.util.Collection;

public class UserDirectoryAdapter extends RecyclerView.Adapter<UserSummaryViewHolder> {
    /*
    SortedList is magic
     */
    @NonNull
    private final SortedList<UserSummary> userSummaries;

    /*
    Logic for handling click events should live outside the view holder, hence a callback interface
     */
    @NonNull
    private final UserSummaryListener listener;

    public UserDirectoryAdapter(
            @NonNull UserSummaryListener listener
    ) {
        this.userSummaries = new SortedList<>(UserSummary.class, new UserDirectoryAdapterCallback(this));
        this.listener = listener;
    }

    /*
    SortedList will perform add and remove operations on its existing dataset to
    make it match the data set passed in to replaceAll. These will call granular
    notifyItemChanged/Inserted/Moved/Removed methods instead of notifyDataSetChanged,
    so its more performant and the UI is more pleasant for users
     */
    public void addUserSummaries(
            @NonNull Collection<UserSummary> userSummary
    ) {
        this.userSummaries.replaceAll(userSummary);
    }

    @NonNull
    @Override
    public UserSummaryViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent,
            int viewType
    ) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_summary, parent, false);
        return new UserSummaryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull UserSummaryViewHolder holder,
            int position
    ) {
        UserSummary userSummary = userSummaries.get(position);
        holder.bind(userSummary, listener);
    }

    @Override
    public int getItemCount() {
        return userSummaries.size();
    }
}
