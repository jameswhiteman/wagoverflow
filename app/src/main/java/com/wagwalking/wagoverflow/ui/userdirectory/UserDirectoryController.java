package com.wagwalking.wagoverflow.ui.userdirectory;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.wagwalking.models.StackExchangeUser;
import com.wagwalking.wagoverflow.core.LifecycleEvent;
import com.wagwalking.wagoverflow.core.WagOverflowController;
import com.wagwalking.wagoverflow.repositories.UserRepository;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummary;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummaryFactory;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummaryListener;

import java.util.Collection;
import java.util.HashSet;

/*
Business logic live here for the user directory screen
 */
public class UserDirectoryController implements WagOverflowController, UserSummaryListener {
    @NonNull
    private final UserDirectoryView view;

    @NonNull
    private final UserRepository userRepository;

    @NonNull
    private final UserSummaryFactory userSummaryFactory;

    @NonNull
    private final UserDirectoryAdapter adapter;

    public UserDirectoryController(
            @NonNull UserDirectoryView view,
            @NonNull UserRepository userRepository,
            @NonNull UserSummaryFactory userSummaryFactory
    ) {
        this.view = view;
        this.userRepository = userRepository;
        this.userSummaryFactory = userSummaryFactory;
        this.adapter = new UserDirectoryAdapter(this);
    }

    /*
    When the activity is created or restoring (after configuration change),
    show loading in the UI and load the users from the repository.
     */
    @Override
    public void onLifecycleEvent(
            @NonNull LifecycleEvent event,
            @Nullable Bundle state
    ) {
        switch (event) {
            case CREATE:
            case RESTORE:
                view.showProgressIndicator();
                view.hideUsers();
                loadStackExchangeUsers();
                break;
            default:
                break;
        }
    }

    @Override
    public void saveState(
            @NonNull Bundle state
    ) {
        // Nothing to do at the moment, but in the future would do stuff here and in the
        // restore case above to ensure the user's exact state on the page is preserved
        // across configuration changes

        // Users are already cached due to application scoped User Repository so they load
        // faster from the repo call than doing serialization/deserialization into this
        // bundle
    }

    /*
    When clicking on a user, open up their stack exchange page on the internet browser
     */
    @Override
    public void onUserSummaryClicked(@NonNull UserSummary userSummary) {
        String visitUrl = userSummary.getVisitUrl();

        if (visitUrl != null) {
            view.shareUrlToSystem(visitUrl);
        }
    }

    /*
    When users load from the repo, show them in the list and hide the loading / error states.
    When they fail, show the error notice to the user and hide the list until a successful reload.
     */
    private void loadStackExchangeUsers() {
        userRepository.getStackExchangeUsers(new UserRepository.StackExchangeUserCallback() {
            @Override
            public void onUsersLoaded(@NonNull Collection<StackExchangeUser> stackExchangeUsers) {
                Collection<UserSummary> userSummaries = new HashSet<>();

                for (StackExchangeUser user : stackExchangeUsers) {
                    UserSummary userSummary = userSummaryFactory.create(user);
                    userSummaries.add(userSummary);
                }

                adapter.addUserSummaries(userSummaries);

                view.hideProgressIndicator();
                view.showUsers(adapter);
                view.hideNetworkErrorNotice();
            }

            @Override
            public void onError() {
                view.showNetworkErrorNotice();
                view.showProgressIndicator();
                view.hideUsers();
            }
        });
    }
}
