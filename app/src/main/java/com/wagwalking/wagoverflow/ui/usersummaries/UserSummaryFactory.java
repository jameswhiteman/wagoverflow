package com.wagwalking.wagoverflow.ui.usersummaries;

import android.support.annotation.NonNull;

import com.wagwalking.models.StackExchangeBadgeSet;
import com.wagwalking.models.StackExchangeUser;

/*
Create UserSummaries from different sources. For now, just a StackExchangeUser, maybe
a Wag! user in the future or something though.
 */
public class UserSummaryFactory {
    @NonNull
    public UserSummary create(
            @NonNull StackExchangeUser user
    ) {
        StackExchangeBadgeSet badges = user.getBadges();
        int goldCount = badges != null ? badges.getGold() : 0;
        int silverCount = badges != null ? badges.getSilver() : 0;
        int bronzeCount = badges != null ? badges.getBronze() : 0;

        return new UserSummary(
                user.getUserId(),
                user.getDisplayName(),
                user.getProfileImageUrl(),
                user.getStackExchangeUrl(),
                goldCount,
                silverCount,
                bronzeCount
        );
    }
}
