package com.wagwalking.wagoverflow.ui.usersummaries;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wagwalking.wagoverflow.R;
import com.wagwalking.wagoverflow.core.GlideApp;

public class UserSummaryViewHolder extends RecyclerView.ViewHolder {
    @NonNull
    private final ImageView avatar;

    @NonNull
    private final TextView name;

    @NonNull
    private final TextView goldBadge;

    @NonNull
    private final TextView silverBadge;

    @NonNull
    private final TextView bronzeBadge;

    @Nullable
    private final ImageSpan goldBadgeSpan;

    @Nullable
    private final ImageSpan silverBadgeSpan;

    @Nullable
    private final ImageSpan bronzeBadgeSpan;

    public UserSummaryViewHolder(
            @NonNull View itemView
    ) {
        super(itemView);
        avatar = itemView.findViewById(R.id.avatar);
        name = itemView.findViewById(R.id.name);
        goldBadge = itemView.findViewById(R.id.gold_badge);
        silverBadge = itemView.findViewById(R.id.silver_badge);
        bronzeBadge = itemView.findViewById(R.id.bronze_badge);

        // Caching image spans at construction will prevent having to reload drawables and create
        // spans at bind time, reducing jank.
        Context context = itemView.getContext();

        Drawable goldBadgeDrawable = ContextCompat.getDrawable(context, R.drawable.badge);
        Drawable silverBadgeDrawable = ContextCompat.getDrawable(context, R.drawable.badge);
        Drawable bronzeBadgeDrawable = ContextCompat.getDrawable(context, R.drawable.badge);


        if (goldBadgeDrawable != null) {
            goldBadgeDrawable.setBounds(0, 0, goldBadgeDrawable.getIntrinsicWidth(), goldBadgeDrawable.getIntrinsicHeight());
            @ColorInt int goldColorValue = ContextCompat.getColor(context, R.color.goldBadge);
            goldBadgeDrawable.setColorFilter(goldColorValue, PorterDuff.Mode.SRC_ATOP);
            goldBadgeSpan = new ImageSpan(goldBadgeDrawable, ImageSpan.ALIGN_BASELINE);
        } else {
            goldBadgeSpan = null;
        }

        if (silverBadgeDrawable != null) {
            silverBadgeDrawable.setBounds(0, 0, silverBadgeDrawable.getIntrinsicWidth(), silverBadgeDrawable.getIntrinsicHeight());
            @ColorInt int silverColorValue = ContextCompat.getColor(context, R.color.silverBadge);
            silverBadgeDrawable.setColorFilter(silverColorValue, PorterDuff.Mode.SRC_ATOP);
            silverBadgeSpan = new ImageSpan(silverBadgeDrawable, ImageSpan.ALIGN_BASELINE);
        } else {
            silverBadgeSpan = null;
        }

        if (bronzeBadgeDrawable != null) {
            bronzeBadgeDrawable.setBounds(0, 0, bronzeBadgeDrawable.getIntrinsicWidth(), bronzeBadgeDrawable.getIntrinsicHeight());
            @ColorInt int bronzeColorValue = ContextCompat.getColor(context, R.color.bronzeBadge);
            bronzeBadgeDrawable.setColorFilter(bronzeColorValue, PorterDuff.Mode.SRC_ATOP);
            bronzeBadgeSpan = new ImageSpan(bronzeBadgeDrawable, ImageSpan.ALIGN_BASELINE);
        } else {
            bronzeBadgeSpan = null;
        }
    }

    public void bind(
            @NonNull final UserSummary userSummary,
            @NonNull final UserSummaryListener listener
    ) {
        Context context = itemView.getContext();

        // Name
        name.setText(userSummary.getName());

        // Avatar
        // Glide takes care of the heavy lifting of caching images intelligently
        // Placeholder images specified will show when image requests are still loading
        String avatarUrl = userSummary.getAvatarUrl();
        GlideApp.with(context)
                .load(avatarUrl)
                .placeholder(R.drawable.ic_person_black_24dp)
                .fallback(R.drawable.ic_person_black_24dp)
                .error(R.drawable.ic_sentiment_very_dissatisfied_black_24dp)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(avatar);

        // Badges
        String space = "  ";
        String goldBadgeCount = String.valueOf(userSummary.getGoldBadgeCount());
        String silverBadgeCount = String.valueOf(userSummary.getSilverBadgeCount());
        String bronzeBadgeCount = String.valueOf(userSummary.getBronzeBadgeCount());

        Spannable goldBadgeText = new SpannableStringBuilder(space).append(goldBadgeCount);
        Spannable silverBadgeText = new SpannableStringBuilder(space).append(silverBadgeCount);
        Spannable bronzeBadgeText = new SpannableStringBuilder(space).append(bronzeBadgeCount);

        goldBadgeText.setSpan(goldBadgeSpan, 0, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        silverBadgeText.setSpan(silverBadgeSpan, 0, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        bronzeBadgeText.setSpan(bronzeBadgeSpan, 0, 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        goldBadge.setText(goldBadgeText);
        silverBadge.setText(silverBadgeText);
        bronzeBadge.setText(bronzeBadgeText);

        // Click Listener
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onUserSummaryClicked(userSummary);
            }
        });
    }
}
