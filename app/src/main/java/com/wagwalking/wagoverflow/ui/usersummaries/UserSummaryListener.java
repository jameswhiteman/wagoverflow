package com.wagwalking.wagoverflow.ui.usersummaries;

import android.support.annotation.NonNull;

public interface UserSummaryListener {
    void onUserSummaryClicked(@NonNull UserSummary userSummary);
}
