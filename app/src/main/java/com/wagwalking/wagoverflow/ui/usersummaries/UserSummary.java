package com.wagwalking.wagoverflow.ui.usersummaries;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/*
The data representation of a single item in the recycler view of the User Directory page.
By being oblivious to the backing data type for this item (in this case it'll always be
a StackExchangeUser), we can maximize reusability of the view holder, so if we add support
for a Wag! user later they can live in the same UI still, just generating UserSummary items
from a different initial source.
 */
public class UserSummary {
    @NonNull
    private final String id;

    @Nullable
    private final String name;

    @Nullable
    private final String avatarUrl;

    @Nullable
    private final String visitUrl;

    private final int goldBadgeCount;
    private final int silverBadgeCount;
    private final int bronzeBadgeCount;

    public UserSummary(
            @NonNull String id,
            @Nullable String name,
            @Nullable String avatarUrl,
            @Nullable String visitUrl,
            int goldBadgeCount,
            int silverBadgeCount,
            int bronzeBadgeCount
    ) {
        this.id = id;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.visitUrl = visitUrl;
        this.goldBadgeCount = goldBadgeCount;
        this.silverBadgeCount = silverBadgeCount;
        this.bronzeBadgeCount = bronzeBadgeCount;
    }

    @NonNull
    private String getId() {
        return id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getAvatarUrl() {
        return avatarUrl;
    }

    @Nullable
    public String getVisitUrl() {
        return visitUrl;
    }

    public int getGoldBadgeCount() {
        return goldBadgeCount;
    }

    public int getSilverBadgeCount() {
        return silverBadgeCount;
    }

    public int getBronzeBadgeCount() {
        return bronzeBadgeCount;
    }

    /*
    Can do some nice recyclerview performance optimizations down the line with this
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /*
    ID equality is all we care about
     */
    @Override
    public boolean equals(
            @Nullable Object object
    ) {
        final boolean equal;

        if (object != null && object instanceof UserSummary) {
            UserSummary other = (UserSummary)object;
            equal = other.getId().equals(id);
        } else {
            equal = false;
        }

        return equal;
    }

    /*
    Things that are displayed are all we care about
     */
    public boolean sameContents(
            @NonNull UserSummary other
    ) {
        String otherName = other.getName();
        String otherAvatarUrl = other.getAvatarUrl();

        boolean sameName = (otherName == null && name == null) ||
                (otherName != null && name != null && otherName.equals(name));
        boolean sameAvatarUrl = (otherAvatarUrl == null && avatarUrl == null) ||
                (otherAvatarUrl != null && avatarUrl != null && otherAvatarUrl.equals(avatarUrl));
        boolean sameGold = other.getGoldBadgeCount() == goldBadgeCount;
        boolean sameSilver = other.getSilverBadgeCount() == silverBadgeCount;
        boolean sameBronze = other.getBronzeBadgeCount() == bronzeBadgeCount;
        return sameName && sameAvatarUrl && sameGold && sameSilver && sameBronze;
    }
}
