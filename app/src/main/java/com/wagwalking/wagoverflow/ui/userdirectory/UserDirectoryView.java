package com.wagwalking.wagoverflow.ui.userdirectory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wagwalking.wagoverflow.R;

/*
Lots of folks put this in the activity but I like having another class personally.
Makes it feel cleaner to me, since all the views can be non-null and final, and
there's no weird forwarding type code to the controller interspersed with the pure UI
code.
 */
public class UserDirectoryView {
    @NonNull
    private final Context context;

    @NonNull
    private final View progressIndicator;

    @NonNull
    private final RecyclerView users;

    @NonNull
    private final TextView notice;

    public UserDirectoryView(@NonNull View view) {
        context = view.getContext();
        progressIndicator = view.findViewById(R.id.progress_indicator);
        users = view.findViewById(R.id.items);
        notice = view.findViewById(R.id.notice);
        users.setHasFixedSize(true);
    }

    public void showProgressIndicator() {
        progressIndicator.setVisibility(View.VISIBLE);
    }

    public void hideProgressIndicator() {
        progressIndicator.setVisibility(View.GONE);
    }

    public void showUsers(@NonNull RecyclerView.Adapter adapter) {
        users.setVisibility(View.VISIBLE);

        if (users.getAdapter() != adapter) {
            users.setAdapter(adapter);
        }
    }

    public void hideUsers() {
        users.setVisibility(View.GONE);
    }

    public void showNetworkErrorNotice() {
        notice.setText(R.string.notice_stack_exchange_user_error);
        notice.setVisibility(View.VISIBLE);
    }

    public void hideNetworkErrorNotice() {
        notice.setVisibility(View.GONE);
    }

    public void shareUrlToSystem(
            @NonNull String url
    ) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        context.startActivity(intent);
    }
}
