package com.wagwalking.wagoverflow.ui.userdirectory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.wagwalking.wagoverflow.R;
import com.wagwalking.wagoverflow.core.WagOverflowActivity;
import com.wagwalking.wagoverflow.core.WagOverflowController;
import com.wagwalking.wagoverflow.repositories.UserRepository;
import com.wagwalking.wagoverflow.ui.usersummaries.UserSummaryFactory;

public class UserDirectoryActivity extends WagOverflowActivity {
    @NonNull
    @Override
    protected WagOverflowController createController(
            @NonNull Context context,
            @NonNull View rootView
    ) {
        UserDirectoryView view = new UserDirectoryView(rootView);
        UserRepository userRepository = UserRepository.getInstance();
        UserSummaryFactory userSummaryFactory = new UserSummaryFactory();
        return new UserDirectoryController(view, userRepository, userSummaryFactory);
    }

    @Nullable
    @Override
    protected Integer getLayoutResId() {
        return R.layout.user_directory;
    }
}
