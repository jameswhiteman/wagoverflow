package com.wagwalking.wagoverflow.ui.userdirectory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import com.wagwalking.wagoverflow.ui.usersummaries.UserSummary;

public class UserDirectoryAdapterCallback extends SortedListAdapterCallback<UserSummary> {
    public UserDirectoryAdapterCallback(RecyclerView.Adapter adapter) {
        super(adapter);
    }

    /*
    Sorting users alphabetically in the list.
     */
    @Override
    public int compare(
            @NonNull UserSummary a,
            @NonNull UserSummary b
    ) {
        String nameA = a.getName();
        String nameB = b.getName();
        final int comparison;

        if (nameA == null && nameB == null) {
            comparison = 0;
        } else if (nameA == null) {
            comparison = 1;
        } else if (nameB == null) {
            comparison = -1;
        } else {
            comparison = nameA.compareTo(nameB);
        }

        return comparison;
    }

    /*
    Returning false will rebind the view holder corresponding to the user summary item,
    so only things which effect the binding of the view holder need to be checked here.
     */
    @Override
    public boolean areContentsTheSame(
            @NonNull UserSummary oldItem,
            @NonNull UserSummary newItem
    ) {
        return oldItem.sameContents(newItem);
    }

    /*
    Used to de-duplicate items in the Sorted List
     */
    @Override
    public boolean areItemsTheSame(
            @NonNull UserSummary a,
            @NonNull UserSummary b
    ) {
        return a.equals(b);
    }
}
