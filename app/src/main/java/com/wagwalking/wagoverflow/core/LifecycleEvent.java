package com.wagwalking.wagoverflow.core;

public enum LifecycleEvent {
    CREATE, RESTORE, START, RESUME, PAUSE, STOP, DESTROY
}
