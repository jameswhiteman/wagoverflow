package com.wagwalking.wagoverflow.core;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface WagOverflowController {
    void onLifecycleEvent(
            @NonNull LifecycleEvent event,
            @Nullable Bundle state
    );

    void saveState(
            @NonNull Bundle bundle
    );
}
