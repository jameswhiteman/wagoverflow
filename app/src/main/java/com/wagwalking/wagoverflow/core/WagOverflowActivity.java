package com.wagwalking.wagoverflow.core;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public abstract class WagOverflowActivity extends AppCompatActivity {
    /*
    Having a plain old Java object as the center for a screen's logic makes
    unit testing much less painful in the future. Activities will delegate
    lifecycle events and such to controllers.
     */
    @Nullable
    private WagOverflowController controller;

    @NonNull
    protected abstract WagOverflowController createController(
            @NonNull Context context,
            @NonNull View view
    );

    @Nullable
    protected abstract Integer getLayoutResId();

    @Override
    protected void onCreate(
            Bundle savedInstanceState
    ) {
        super.onCreate(savedInstanceState);
        Integer layoutResId = getLayoutResId();

        if (layoutResId != null) {
            setContentView(layoutResId);
        }

        View rootView = findViewById(android.R.id.content);
        controller = createController(this, rootView);

        if (savedInstanceState == null) {
            controller.onLifecycleEvent(LifecycleEvent.CREATE, null);
        } else {
            controller.onLifecycleEvent(LifecycleEvent.RESTORE, savedInstanceState);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (controller != null) {
            controller.onLifecycleEvent(LifecycleEvent.START, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (controller != null) {
            controller.onLifecycleEvent(LifecycleEvent.RESUME, null);
        }
    }

    @Override
    protected void onPause() {
        if (controller != null) {
            controller.onLifecycleEvent(LifecycleEvent.PAUSE, null);
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (controller != null) {
            controller.onLifecycleEvent(LifecycleEvent.STOP, null);
        }

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (controller != null) {
            controller.onLifecycleEvent(LifecycleEvent.DESTROY, null);
        }

        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (controller != null) {
            controller.saveState(outState);
        }
    }
}
