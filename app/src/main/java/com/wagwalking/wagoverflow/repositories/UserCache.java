package com.wagwalking.wagoverflow.repositories;

import android.support.annotation.Nullable;

import com.wagwalking.models.StackExchangeUser;

import java.util.Collection;

/*
Very simple right now but as caching and indexing needs
increase in the future we could add in some hash maps
on whatever we want to key on into here.
 */
public class UserCache {
    @Nullable
    private Collection<StackExchangeUser> stackExchangeUsers;

    public void setStackExchangeUsers(
            @Nullable Collection<StackExchangeUser> stackExchangeUsers
    ) {
        this.stackExchangeUsers = stackExchangeUsers;
    }

    @Nullable
    public Collection<StackExchangeUser> getStackExchangeUsers() {
        return stackExchangeUsers;
    }
}
