package com.wagwalking.wagoverflow.repositories;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.wagwalking.models.StackExchangeUser;
import com.wagwalking.wagoverflow.network.StackExchangeRestService;
import com.wagwalking.wagoverflow.network.StackExchangeWebservice;

import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;

/*
Mediator between network and caching layer. In the future, persistence
layer too would be mediated here.
 */
public class UserRepository {
    @NonNull
    private final StackExchangeWebservice stackExchangeWebservice;

    @NonNull
    private final UserCache userCache;

    @NonNull
    private final Handler mainThreadHandler;

    @NonNull
    private final Executor executor;

    @Nullable
    private static UserRepository INSTANCE;

    /*
    Singleton scoped for now so network calls survive
    regardless of activity lifecycle, instead they
    survive by application lifecycle.
     */
    @NonNull
    public static UserRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserRepository();
        }

        return INSTANCE;
    }

    /*
    In Kotlin, optional parameters make these annoying telescoping constructors
    a thing of the past
     */
    public UserRepository() {
        this(new StackExchangeRestService(new OkHttpClient(), new Gson()),
                new UserCache(),
                new Handler(Looper.getMainLooper()),
                Executors.newSingleThreadExecutor()
        );
    }

    public UserRepository(
            @NonNull StackExchangeWebservice stackExchangeWebservice,
            @NonNull UserCache userCache,
            @NonNull Handler mainThreadHandler,
            @NonNull Executor executor
    ) {
        this.stackExchangeWebservice = stackExchangeWebservice;
        this.userCache = userCache;
        this.mainThreadHandler = mainThreadHandler;
        this.executor = executor;
    }

    /*
    Run network request in executor so as not to block main thread which calls this method.
    Try and retrieve from in memory cache first any users that have been loaded already.
    If users come back null from webservice it indicates an error (would definitely want a
    more pristine error handling mechanism in the future), so we trigger a retry and notify
    the UI that an error has happened. If successful, also notifies UI with users.
     */
    public void getStackExchangeUsers(
            @NonNull final StackExchangeUserCallback callback
    ) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                final Collection<StackExchangeUser> oldUsers = userCache.getStackExchangeUsers();

                if (oldUsers != null) {
                    notifyListener(oldUsers, callback);
                } else {
                    final Collection<StackExchangeUser> newUsers = stackExchangeWebservice.getAllStackExchangeUsers();

                    if (newUsers == null) {
                        retry(10000, new RetryCallback() {
                            @Override
                            public void onRetry() {
                                getStackExchangeUsers(callback);
                            }
                        });
                    } else {
                        userCache.setStackExchangeUsers(newUsers);
                    }

                    notifyListener(newUsers, callback);
                }
            }
        });
    }

    /*
    Ensures notifying the listener happens on the main thread, as attempting
    to modify the UI from outside the main thread will crash everything
     */
    private void notifyListener(
            @Nullable final Collection<StackExchangeUser> stackExchangeUsers,
            @NonNull final StackExchangeUserCallback callback
    ) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                if (stackExchangeUsers == null) {
                    callback.onError();
                } else {
                    callback.onUsersLoaded(stackExchangeUsers);
                }
            }
        });
    }

    /*
    Schedule a retry. Since repository is application scoped, this timer survives
    with the application, is unaffected by activity lifecycle.
     */
    private void retry(
            long delayMillis,
            @NonNull final RetryCallback callback
    ) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                callback.onRetry();
            }
        }, delayMillis);
    }

    private interface RetryCallback {
        void onRetry();
    }

    public interface StackExchangeUserCallback {
        void onUsersLoaded(
                @NonNull Collection<StackExchangeUser> stackExchangeUsers
        );

        void onError();
    }
}
