package com.wagwalking.wagoverflow.network;

import android.support.annotation.Nullable;

import com.wagwalking.models.StackExchangeUser;

import java.util.Collection;

/*
If we wanna switch off OK HTTP later or migrate to SOAP or slot in some mock
webservices this interface will be a blessing.
 */
public interface StackExchangeWebservice {
    @Nullable
    Collection<StackExchangeUser> getAllStackExchangeUsers();
}
