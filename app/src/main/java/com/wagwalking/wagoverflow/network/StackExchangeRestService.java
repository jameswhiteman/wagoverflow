package com.wagwalking.wagoverflow.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.wagwalking.models.StackExchangeUser;
import com.wagwalking.models.StackExchangeWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class StackExchangeRestService implements StackExchangeWebservice {
    @NonNull
    private static final String TAG = "StackExchangeRS";
    @NonNull
    private final OkHttpClient client;

    @NonNull
    private final Gson gson;

    public StackExchangeRestService(
            @NonNull OkHttpClient okHttpClient,
            @NonNull Gson gson
    ) {
        this.client = okHttpClient;
        this.gson = gson;
    }

    /*
    Could do better error handling in the future but this gets us to a good place for now.
    We pay attention to Stack Exchange's Backoff integer not to its fullest potential but
    to at least trigger the 10-seconds later retry case in our user repository. Use GSON
    to parse the response into Stack Exchange users when successful and return them directly.
     */
    @Override
    @WorkerThread
    @Nullable
    public Collection<StackExchangeUser> getAllStackExchangeUsers() {
        List<StackExchangeUser> users = null;

        Request request = new Request.Builder()
                .get()
                .url("https://api.stackexchange.com/2.2/users?site=stackoverflow")
                .build();

        try {
            Response response = client.newCall(request).execute();
            ResponseBody body = response.body();

            if (body != null) {
                String rawResponse = body.string();
                StackExchangeWrapper wrapper = gson.fromJson(rawResponse, StackExchangeWrapper.class);

                Integer backoff = wrapper.getBackoff();

                if (backoff != null && backoff > 0) {
                    Log.e(TAG, "Stack Overflow API says we're hammering it on user fetch");
                    return users = null;
                } else {
                    JsonArray items = wrapper.getItems();
                    users = new ArrayList<>();

                    for (JsonElement element : items) {
                        StackExchangeUser user = gson.fromJson(element, StackExchangeUser.class);
                        users.add(user);
                    }
                }
            }
        } catch (IOException | IllegalStateException e) {
            Log.e(TAG, "Error parsing response", e);
            users = null;
        }

        return users;
    }
}
