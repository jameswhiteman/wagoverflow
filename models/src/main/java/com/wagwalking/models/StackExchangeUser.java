package com.wagwalking.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StackExchangeUser implements Serializable {
    @NonNull
    @SerializedName("user_id")
    private final String userId;

    @Nullable
    @SerializedName("display_name")
    private final String displayName;

    @Nullable
    @SerializedName("profile_image")
    private final String profileImageUrl;

    @Nullable
    @SerializedName("link")
    private final String stackExchangeUrl;

    @Nullable
    @SerializedName("badge_counts")
    private final StackExchangeBadgeSet badges;

    public StackExchangeUser(
            @NonNull String userId,
            @Nullable String displayName,
            @Nullable String profileImageUrl,
            @Nullable String stackExchangeUrl,
            @Nullable StackExchangeBadgeSet badges
    ) {
        this.userId = userId;
        this.displayName = displayName;
        this.profileImageUrl = profileImageUrl;
        this.stackExchangeUrl = stackExchangeUrl;
        this.badges = badges;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    @Nullable
    public String getStackExchangeUrl() {
        return stackExchangeUrl;
    }

    @Nullable
    public StackExchangeBadgeSet getBadges() {
        return badges;
    }

    @Override
    public int hashCode() {
        return userId.hashCode();
    }

    @Override
    public boolean equals(
            @Nullable Object object
    ) {
        final boolean equal;

        if (object != null && object instanceof StackExchangeUser) {
            StackExchangeUser other = (StackExchangeUser)object;
            equal = other.getUserId().equals(userId);
        } else {
            equal = false;
        }

        return equal;
    }
}
