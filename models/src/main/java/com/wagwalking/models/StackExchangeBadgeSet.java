package com.wagwalking.models;

public class StackExchangeBadgeSet {
    private final int gold;
    private final int silver;
    private final int bronze;

    public StackExchangeBadgeSet(
            int gold,
            int silver,
            int bronze
    ) {
        this.gold = gold;
        this.silver = silver;
        this.bronze = bronze;
    }

    public int getGold() {
        return gold;
    }

    public int getSilver() {
        return silver;
    }

    public int getBronze() {
        return bronze;
    }
}
