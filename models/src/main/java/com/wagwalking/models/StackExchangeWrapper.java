package com.wagwalking.models;

import android.support.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

public class StackExchangeWrapper {
    @Nullable
    @SerializedName("backoff")
    private Integer backoff;

    @Nullable
    @SerializedName("error_id")
    private Integer errorId;

    @Nullable
    private JsonArray items;

    public StackExchangeWrapper(
            @Nullable Integer backoff,
            @Nullable Integer errorId,
            @Nullable JsonArray items
    ) {
        this.backoff = backoff;
        this.errorId = errorId;
        this.items = items;
    }

    @Nullable
    public Integer getBackoff() {
        return backoff;
    }

    @Nullable
    public Integer getErrorId() {
        return errorId;
    }

    @Nullable
    public JsonArray getItems() {
        return items;
    }
}
